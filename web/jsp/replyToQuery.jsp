<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "//W3C/DTD XHTML 1.1//EN" "http://www.w3.org/xhtml 11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title> REPLY </title>
<style type="text/css">
    html, body {
        background: url(img/bg-img.jpg) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        position: absolute;
        top: 25%;
        left: 25%;
        /*width: 200px;*/
        height: 200px;
        margin: -100px 0 0 -100px;
    }
    input, body, table {
        font-size: 20px;
    }
</style>

<body background="/images/bg-img.jpg">
<form method="post" enctype="multipart/form-data" action="ExpertController">
    <input type="hidden" name="queryId" id="queryId" value="<%= request.getParameter("queryId")%>"/>
    <input type="hidden" name="requestType" id="requestType" value="saveSolution"/>
    Solution<br/><textarea rows="8" cols="100" name="solutionText"></textarea><br/>
    Attach Image<input type="file" name="solutionImage" accept="image/jpeg"/><br/>
    Attach Audio<input type="file" name="solutionAudio" accept="audio/mp3"/><br/>
    <input type="submit" value="Submit solution"/>
</form>
</body>
</html>

