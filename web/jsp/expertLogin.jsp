<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "//W3C/DTD XHTML 1.1//EN" "http://www.w3.org/xhtml 11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title> Expert Login </title>
<style type="text/css">
  input, body {
    font-size: 20px;
  }

  html, body {
    background: url(img/bg-img.jpg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    -webkit-font-smoothing: subpixel-antialiased;
    position: absolute;
    top: 50%;
    left: 50%;
    width: 200px;
    height: 200px;
    margin: -100px 0 0 -100px;
  }
</style>
<body>

<form method="post" action="ExpertController">
  <input type="hidden" name="requestType" id="requestType" value="expertLogin"/>
  Username <input type="text" name="username"/><br/>
  Password <input type="password" name="password"/><br/>
  <input type="submit" value="Login"/>
</form>
</body>
</html>

