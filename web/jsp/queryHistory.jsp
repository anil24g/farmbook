<%@ page import="com.farmbook.model.FarmerQuery" %>
<%@ page import="java.util.List" %>
<%@ page import="com.farmbook.util.Constants" %>
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "//W3C/DTD XHTML 1.1//EN" "http://www.w3.org/xhtml 11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title> FARMDOC </title>
<style type="text/css">
  h1 {text-align: center;
    text-decoration : underline;
  }
  h2 {text-align: left;
    text-decoration : underline;
    color:rgb(0,0,100);
    font-style: italic;}
  .g{text-decoration : underline;
  }
  .rt{text-align: right;
    font-style: italic;}
  html, body {
    background: url(img/bg-img.jpg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
  }
  input, body, table {
    font-size: 20px;
  }
</style>
<body>

<%
  List<FarmerQuery> historyQueries = (List<FarmerQuery>) request.getAttribute("historyQueries");
  int historyQueriesSize = historyQueries!=null?historyQueries.size():0;
%>

<h2 class="g">HISTORY</h2>
<p>Showing <%=historyQueriesSize%> Reports</p>
<table border="1">
  <tr>
    <th>REP.NO</th>
    <th>FARMER USERNAME</th>
    <th>QUERY</th>
    <th>TIME</th>
    <th>LOCATION</th>
    <th>IMAGE</th>
    <th>VOICE</th>
    <th>SOLUTION</th>
  </tr>
  <%
    if(historyQueriesSize>0){
      String basePath = request.getScheme()+"://"+request.getServerName()
              +":"+request.getServerPort()+request.getContextPath();
      for (FarmerQuery farmerQuery : historyQueries){

        String location = farmerQuery.getFarmer().getAddress()==null?"Not Available":farmerQuery.getFarmer().getAddress();
        out.println("<tr>");
        out.println("<td>"+farmerQuery.getId()+"</td>");
        out.println("<td>"+farmerQuery.getFarmer().getUsername()+"</td>");
        out.println("<td>"+farmerQuery.getQueryString()+"</td>");
        out.println("<td>"+farmerQuery.getCreatedOn()+"</td>");
        out.println("<td>"+location+"</td>");
        if(farmerQuery.isqImageAvail())
          out.println("<td> <a href='"+basePath+Constants.getQueryImageUrl(farmerQuery.getId())+"' target=\"blank\">Download Image</a></td>");
        else
          out.println("<td> No Image Available </td>");

        if(farmerQuery.isqAudioAvail())
          out.println("<td><audio src='"+basePath+Constants.getQueryAudioUrl(farmerQuery.getId())+"' controls='controls'/>");
        else
          out.println("<td> No Audio Available </td>");
        out.println("<td>"+farmerQuery.getSolution()+"</td>");
      }
    }
  %>
</table>

</body>
</html>