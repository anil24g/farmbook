package com.farmbook.data;

import com.farmbook.model.Expert;
import com.farmbook.model.Farmer;
import com.farmbook.model.FarmerQuery;
import com.farmbook.util.DataFetcher;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ExpertDao {

    public static Expert getExpertByUsernameAndPassword(String username, String password){
        Expert expert=null;
        String query = "select * from experts WHERE username='"+username+ "' and password='"+password+"'";
        try {
            Statement statement = DataFetcher.getDataConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                 expert = getExpertByResult(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return expert;
    }

    public static Expert getExpertById(long id){
        Expert expert=null;
        String query = "select * from experts WHERE id="+id;
        try {
            Statement statement = DataFetcher.getDataConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                expert = getExpertByResult(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return expert;
    }

    private static Expert getExpertByResult(ResultSet resultSet) throws SQLException {
        if(resultSet!=null){
            Expert expert = new Expert();
            expert.setId(resultSet.getLong("id"));
            expert.setName(resultSet.getString("name"));
            expert.setUsername(resultSet.getString("username"));
            expert.setPassword(resultSet.getString("password"));
            return expert;
        }
        return null;
    }
}