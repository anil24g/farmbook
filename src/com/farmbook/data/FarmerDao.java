package com.farmbook.data;

import com.farmbook.model.Farmer;
import com.farmbook.model.FarmerQuery;
import com.farmbook.util.DataFetcher;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class FarmerDao {

    public static Farmer saveFarmer(Farmer farmer){
        if(farmer!=null){
            String query = getQueryForFarmer(farmer);
            try {
                Statement statement = DataFetcher.getDataConnection().createStatement();
                statement.executeUpdate(query);
                return getFarmerByUsernameAndPassword(farmer.getUsername(),farmer.getPassword());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static boolean saveFarmerQuery(FarmerQuery farmerQuery){
        if(farmerQuery!=null){
            String query= getQueryForFarmerQuery(farmerQuery);
            try {
                Statement statement = DataFetcher.getDataConnection().createStatement();
                statement.executeUpdate(query);
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static Farmer getFarmerByUsernameAndPassword(String username, String password){
        Farmer farmer=null;
        String query = "select * from farmer WHERE username='"+username+ "' and password='"+password+"'";
        try {
            Statement statement = DataFetcher.getDataConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                 farmer = getFarmerByResult(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return farmer;
    }

    public static Farmer getFarmerById(long id){
        Farmer farmer=null;
        String query = "select * from farmer WHERE id="+id;
        try {
            Statement statement = DataFetcher.getDataConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                farmer = getFarmerByResult(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return farmer;
    }

    public static FarmerQuery getFarmerQueryById(Long id){
        String query = "select * from farmer_queries WHERE id="+id;
        FarmerQuery farmerQuery = null;
        try {
            Statement statement = DataFetcher.getDataConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                farmerQuery = getFarmerQueryObjectByResult(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return farmerQuery;
    }

    public static List<FarmerQuery> getFarmerQueries(Long farmerId){
        String query = "select * from farmer_queries WHERE farmer_id="+farmerId+" order by id";
        List<FarmerQuery> farmerQueries = new ArrayList<FarmerQuery>();
        try {
            Statement statement = DataFetcher.getDataConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                farmerQueries.add(getFarmerQueryObjectByResult(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return farmerQueries;
    }

    public static List<FarmerQuery> getAllQueries(){
        String query = "select * from farmer_queries order by id";
        List<FarmerQuery> farmerQueries = new ArrayList<FarmerQuery>();
        try {
            Statement statement = DataFetcher.getDataConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                farmerQueries.add(getFarmerQueryObjectByResult(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return farmerQueries;
    }

    public static boolean updateFarmerDevice(Long farmerId, String deviceId){
        String query = "select id from farmer_devices where farmer_id ="+farmerId + " and device_id ='"+deviceId+"'";
        try {
            Statement statement = DataFetcher.getDataConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            Long deviceAvail = null;
            while (resultSet.next()){
                deviceAvail = resultSet.getLong("id");
            }
            if(deviceAvail==null){
                System.out.println("Farmer " + farmerId + " has logged in from new device, saving new device : " + deviceId);
                String insertQuery = "insert into farmer_devices(farmer_id,device_id) values("+farmerId+",'"+deviceId+"')";
                statement.executeUpdate(insertQuery);
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static List<String> getFarmerDevices(long farmerId){
        String query = "select device_id from farmer_devices where farmer_id ="+farmerId;
        List<String> devices = new ArrayList<String>();
        try {
            Statement statement = DataFetcher.getDataConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                if(resultSet.getString("device_id")!=null && !resultSet.getString("device_id").isEmpty())
                    devices.add(resultSet.getString("device_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return devices;
    }

    private static Farmer getFarmerByResult(ResultSet resultSet) throws SQLException {
        if(resultSet!=null){
            Farmer farmer = new Farmer();
            farmer.setId(resultSet.getLong("id"));
            farmer.setName(resultSet.getString("name"));
            farmer.setUsername(resultSet.getString("username"));
            farmer.setPassword(resultSet.getString("password"));
            farmer.setEmailId(resultSet.getString("email_id"));
            farmer.setPhoneNo(resultSet.getString("phone_no"));
            farmer.setAge(resultSet.getInt("age"));
            farmer.setAddress(resultSet.getString("address"));
            farmer.setCrops(resultSet.getString("crops"));
            farmer.setEquipment(resultSet.getString("equipment"));
            farmer.setLocation(resultSet.getString("location"));
            farmer.setGender(resultSet.getString("gender"));
            return farmer;
        }
        return null;
    }

    private static FarmerQuery getFarmerQueryObjectByResult(ResultSet resultSet) throws SQLException {
        if(resultSet!=null){
            FarmerQuery farmerQuery = new FarmerQuery();
            farmerQuery.setId(resultSet.getLong("id"));
            farmerQuery.setFarmerId(resultSet.getLong("farmer_id"));
            farmerQuery.setqAudioAvail(resultSet.getBoolean("query_audio_available"));
            farmerQuery.setqImageAvail(resultSet.getBoolean("query_image_available"));
            farmerQuery.setCreatedOn(resultSet.getDate("created_on"));
            farmerQuery.setQueryString(resultSet.getString("query_string"));
            farmerQuery.setSolution(resultSet.getString("solution"));
            farmerQuery.setExpertId(resultSet.getLong("expert_id"));
            farmerQuery.setsImageAvail(resultSet.getBoolean("solution_image_available"));
            farmerQuery.setsAudioAvail(resultSet.getBoolean("solution_audio_available"));
            farmerQuery.setAnsweredOn(resultSet.getDate("answered_on"));
            return farmerQuery;
        }
        return null;
    }
    private static String getQueryForFarmer(Farmer farmer){
        String insertQuery="INSERT INTO farmer(name,username,password,email_id,phone_no,age,location,address,crops,equipment,gender) VALUES("
                +getQueryValue(farmer.getName())+","+getQueryValue(farmer.getUsername())+","+getQueryValue(farmer.getPassword())+","
                +getQueryValue(farmer.getEmailId())+","+getQueryValue(farmer.getPhoneNo()) +","+farmer.getAge()+","
                +getQueryValue(farmer.getLocation())+","+getQueryValue(farmer.getAddress())+","+getQueryValue(farmer.getCrops())+","
                +getQueryValue(farmer.getEquipment())+","+getQueryValue(farmer.getGender())+")";
        String updateQuery = "update farmer set name="+getQueryValue(farmer.getName())+" ,email_id="+getQueryValue(farmer.getEmailId())
                +" ,phone_no="+getQueryValue(farmer.getPhoneNo()) + " ,age="+farmer.getAge()+" ,location="+getQueryValue(farmer.getLocation())
                +" ,address=" +getQueryValue(farmer.getAddress()) + " ,crops=" +getQueryValue(farmer.getCrops())
                +" ,gender=" + getQueryValue(farmer.getGender()) +" ,equipment=" + getQueryValue(farmer.getEquipment())
                +" where id =" + farmer.getId();

        return farmer.getId()==null? insertQuery:updateQuery;
    }

    private static String getQueryForFarmerQuery(FarmerQuery farmerQuery){
        String insertQuery="INSERT INTO farmer_queries (farmer_id,query_string,query_image_available," +
                "query_audio_available,created_on,expert_id,answered_on,solution_image_available,solution_audio_available,solution) " +
                "VALUES ("+farmerQuery.getFarmerId()+","+getQueryValue(farmerQuery.getQueryString())+","+farmerQuery.isqImageAvail()+","+farmerQuery.isqAudioAvail()
                +","+getQueryValue(farmerQuery.getCreatedOn()==null?null: new Date(farmerQuery.getCreatedOn().getTime()).toString())+","+farmerQuery.getExpertId()
                +","+getQueryValue(farmerQuery.getAnsweredOn()!=null?new Date(farmerQuery.getAnsweredOn().getTime()).toString():null)
                +","+farmerQuery.issImageAvail()+","+farmerQuery.issAudioAvail()+","+getQueryValue(farmerQuery.getSolution())+");";
        String updateQuery = "update farmer_queries set expert_id="+farmerQuery.getExpertId()+" ,answered_on="
                +getQueryValue(farmerQuery.getAnsweredOn() != null ? new Date(farmerQuery.getAnsweredOn().getTime()).toString() : null)
                +" ,solution_image_available="+farmerQuery.issImageAvail()+" ,solution_audio_available="
                +farmerQuery.issAudioAvail()+" ,solution="+getQueryValue(farmerQuery.getSolution())+" where id = "+farmerQuery.getId();
        System.out.println("query : "+farmerQuery.getId()==null? insertQuery:updateQuery);
        return farmerQuery.getId()==null? insertQuery:updateQuery;
    }

    private static String getQueryValue(String s){
        return s==null? s : "'"+s+"'";
    }
}