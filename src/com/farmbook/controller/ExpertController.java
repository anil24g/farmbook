package com.farmbook.controller;

import com.farmbook.data.ExpertDao;
import com.farmbook.data.FarmerDao;
import com.farmbook.model.Expert;
import com.farmbook.model.FarmerQuery;
import com.farmbook.model.PushNotificationData;
import com.farmbook.model.PushNotificationRequest;
import com.farmbook.util.Constants;
import com.farmbook.util.PushNotificationTask;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@MultipartConfig
public class ExpertController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestType = req.getParameter("requestType");
        boolean loggedInUser = false;
        Expert expert = (Expert) req.getSession().getAttribute("expert");
        if(expert!=null && expert.getId()!=null){
            loggedInUser = true;
        }
        System.out.println("Request Type : "+requestType+" loggedInUser : "+loggedInUser);
        if(requestType!=null && !requestType.isEmpty()){
            if(requestType.equals("expertLogin")){
                loggedInUser = doExpertLogin(req,resp);
            }
            if(loggedInUser){
                if(requestType.equals("replyToQuery")){
                    replyToQuery(req, resp);
                    return;
                } else if(requestType.equals("saveSolution")){
                    saveSolution(req, resp);
                    return;
                } else if (requestType.equals("historyQuery")){
                    showAnsweredQueries(req,resp);
                    return;
                }
            }
        }

        if(!loggedInUser) {
            showExpertLogin(req,resp);
        } else
            showUnansweredQueries(req,resp);
    }

    private void showUnansweredQueries(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<FarmerQuery> allQueries = FarmerDao.getAllQueries();
        List<FarmerQuery> unAnsweredQuery = new ArrayList<>();
        for (FarmerQuery farmerQuery : allQueries) {
            farmerQuery.setFarmer(FarmerDao.getFarmerById(farmerQuery.getFarmerId()));
            if(farmerQuery.getAnsweredOn()==null)
                unAnsweredQuery.add(farmerQuery);
        }
        req.setAttribute("queries", unAnsweredQuery);
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/farmerQueries.jsp");
        requestDispatcher.forward(req, resp);
    }

    private void showAnsweredQueries(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<FarmerQuery> allQueries = FarmerDao.getAllQueries();
        List<FarmerQuery> answeredQuery = new ArrayList<>();
        for (FarmerQuery farmerQuery : allQueries) {
            farmerQuery.setFarmer(FarmerDao.getFarmerById(farmerQuery.getFarmerId()));
            if(farmerQuery.getAnsweredOn()!=null)
                answeredQuery.add(farmerQuery);
        }
        req.setAttribute("historyQueries", answeredQuery);
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/queryHistory.jsp");
        requestDispatcher.forward(req, resp);
    }

    private void replyToQuery(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/replyToQuery.jsp");
        requestDispatcher.forward(req, resp);
    }

    private void saveSolution(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long queryId = Long.valueOf(req.getParameter("queryId"));
        String solutionText = req.getParameter("solutionText");
        FarmerQuery farmerQuery = FarmerDao.getFarmerQueryById(queryId);
        farmerQuery.setSolution(solutionText);
        farmerQuery.setAnsweredOn(new Date());

        Expert expert = (Expert) req.getSession().getAttribute("expert");
        if(expert!=null && expert.getId()!=null){
            farmerQuery.setExpertId(expert.getId());
        }

        Part solutionImage = req.getPart("solutionImage");
        if(solutionImage!=null && solutionImage.getSize()>0){
            String sImageLocation = Constants.getSolutionImagePath() + queryId + Constants.IMAGE_FORMAT;
            farmerQuery.setsImageAvail(savePart(solutionImage,sImageLocation));
        }

        Part solutionAudio = req.getPart("solutionAudio");
        if(solutionAudio!=null && solutionAudio.getSize()>0){
            String sAudionLocation = Constants.getSolutionAudioPath() + queryId + Constants.AUDIO_FORMAT;
            farmerQuery.setsAudioAvail(savePart(solutionAudio, sAudionLocation));
        }

        FarmerDao.saveFarmerQuery(farmerQuery);
        notifyFarmer(farmerQuery,farmerQuery.getFarmerId());
        showUnansweredQueries(req, resp);
    }

    private void showExpertLogin(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/expertLogin.jsp");
        requestDispatcher.forward(req, resp);
    }

    private boolean doExpertLogin(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if(username!=null && password!=null && !username.isEmpty() && !password.isEmpty()) {
            Expert expert = ExpertDao.getExpertByUsernameAndPassword(username, password);
            if(expert!=null && expert.getId()!=null){
                req.getSession().setAttribute("expert",expert);
                return true;
            }
        }
        return false;
    }

    private void notifyFarmer(FarmerQuery farmerQuery,long farmer_id){
        List<String> farmerDevices = FarmerDao.getFarmerDevices(farmer_id);
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest();
        pushNotificationRequest.setRegistration_ids(farmerDevices);
        PushNotificationData pushNotificationData =  new PushNotificationData();
        pushNotificationData.setQuery(farmerQuery);
        pushNotificationRequest.setData(pushNotificationData);
        new PushNotificationTask(pushNotificationRequest).run();
    }

    private boolean savePart(Part filePart, String filename){
        OutputStream out = null;
        InputStream filecontent = null;

        try {
            out = new FileOutputStream(new File(filename));
            filecontent = filePart.getInputStream();
            int read = 0;
            final byte[] bytes = new byte[1024];
            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            return true;
        } catch (Exception e){
            System.out.println("Error while saving file " + filename);
            e.printStackTrace();
        } finally {
            if(out!=null)
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return false;
    }
}
