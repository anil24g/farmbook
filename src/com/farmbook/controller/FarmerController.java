package com.farmbook.controller;

import com.farmbook.data.FarmerDao;
import com.farmbook.model.BaseModel;
import com.farmbook.model.Farmer;
import com.farmbook.model.FarmerQuery;
import com.farmbook.util.Constants;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.util.Date;
import java.util.List;

public class FarmerController extends HttpServlet {

    private  Gson gson = new Gson();

    public void doPost(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException
    {
        BufferedReader reader = request.getReader();
        String line;
        StringBuilder buffer = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        String data = buffer.toString();
        BaseModel baseModel = gson.fromJson(data, BaseModel.class);
        Farmer farmer = null;
        FarmerQuery farmerQuery = null;
        String responseString = "{error:\"no response\"}";
        if(baseModel!=null && baseModel.getRequestType()!=null){
            if(baseModel.getRequestType().equals("submitquery"))
                farmerQuery =gson.fromJson(data,FarmerQuery.class);
            else
                farmer = gson.fromJson(data, Farmer.class);
            switch (baseModel.getRequestType()){
                case "signup": responseString = signUpFarmer(farmer); break;
                case "signin": responseString = signInFarmer(farmer); break;
                case "updateprofile": responseString = updateFarmer(farmer); break;
                case "submitquery" : responseString = submitQuery(farmerQuery); break;
                case "getqueries" : responseString = getAllFarmerQueries(farmer); break;
            }

        }

        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.println(responseString);
    }

    private String submitQuery(FarmerQuery farmerQuery) {
        System.out.println(farmerQuery.getQueryString() + " : " + farmerQuery.getImageString() + " : " + farmerQuery.getAudioString());
        String message="";
        Farmer farmer = FarmerDao.getFarmerByUsernameAndPassword(farmerQuery.getUsername(), farmerQuery.getPassword());
        if(farmer!=null && farmer.getId()!=null){
            FarmerQuery farmerQueryRequest = constructFarmerQueryObject(farmer, farmerQuery);
            if(farmerQueryRequest!=null){
                boolean querySaved = FarmerDao.saveFarmerQuery(farmerQueryRequest);
                if(querySaved){
                    List<FarmerQuery> farmerQueries = FarmerDao.getFarmerQueries(farmer.getId());
                    if(farmerQueries!=null && !farmerQueries.isEmpty()){
                        final FarmerQuery savedFarmerQuery = farmerQueries.get(farmerQueries.size() - 1);
                        if(farmerQuery.getImageString()!=null && !farmerQuery.getImageString().isEmpty())
                            saveContentFromString(farmerQuery.getImageString(), Constants.getQueryImagePath() + savedFarmerQuery.getId() + Constants.IMAGE_FORMAT);
                        if(farmerQuery.getAudioString()!=null && !farmerQuery.getAudioString().isEmpty()){
                            saveContentFromString(farmerQuery.getAudioString(), Constants.getQueryAudioPath() + savedFarmerQuery.getId() + Constants.AUDIO_FORMAT);
                        }
                        return (new Gson()).toJson(savedFarmerQuery);
                    }
                }
            } else
                message = "Error in query request";
        } else {
            message = "Invalid username/password";
        }
        return getGenericResponse(message);
    }

    private String getAllFarmerQueries(Farmer farmer){
        if (farmer!=null){
            Farmer loggedInFarmer = FarmerDao.getFarmerByUsernameAndPassword(farmer.getUsername(), farmer.getPassword());
            if(loggedInFarmer!=null && loggedInFarmer.getId()!=null) {
                List<FarmerQuery> farmerQueries = FarmerDao.getFarmerQueries(loggedInFarmer.getId());
                for(FarmerQuery farmerQuery : farmerQueries){
                    if(farmerQuery.isqImageAvail())
                        farmerQuery.setqImageUrl(Constants.getQueryImageUrl(farmerQuery.getId()));
                    if(farmerQuery.isqAudioAvail())
                        farmerQuery.setqAudioUrl(Constants.getQueryAudioUrl(farmerQuery.getId()));
                    if(farmerQuery.issImageAvail())
                        farmerQuery.setsImageUrl(Constants.getSolutionImageUrl(farmerQuery.getId()));
                    if(farmerQuery.issAudioAvail())
                        farmerQuery.setsAudioUrl(Constants.getSolutionAudioUrl(farmerQuery.getId()));
                }
                return gson.toJson(farmerQueries);
            } else return getGenericResponse("Invalid username/password");
        }
        return getGenericResponse("Invalid Farmer Object");
    }

    private String signUpFarmer(Farmer farmer){
        if(farmer!=null && farmer.getId()==null){
            Farmer savedFarmer = FarmerDao.saveFarmer(farmer);
            if(savedFarmer!=null){
                updateFarmerDevices(savedFarmer,farmer.getDeviceId());
                return gson.toJson(savedFarmer);
            }
        }
        return getGenericResponse("");
    }

    private String getGenericResponse(){
        return getGenericResponse("");
    }

    private String getGenericResponse(String message) {
        return getGenericResponse("error", message);
    }

    private String getGenericResponse(String type, String message) {
        message = message==null?"":message;
        return "{\"response\":\""+type+"\",\"message\":\""+message+"\"}";
    }

    private String signInFarmer(Farmer farmer){
        if(farmer!=null){
            Farmer loggedInFarmer = FarmerDao.getFarmerByUsernameAndPassword(farmer.getUsername(), farmer.getPassword());
            if(loggedInFarmer!=null){
                updateFarmerDevices(loggedInFarmer,farmer.getDeviceId());
                return gson.toJson(loggedInFarmer);
            }
        }
        return getGenericResponse("");
    }

    private String updateFarmer(Farmer newFarmer){
        if(newFarmer!=null){
            Farmer oldFarmer = FarmerDao.getFarmerByUsernameAndPassword(newFarmer.getUsername(), newFarmer.getPassword());
            if(oldFarmer!=null && oldFarmer.getId()!=null){
                newFarmer.setId(oldFarmer.getId());
                Farmer savedFarmer = FarmerDao.saveFarmer(newFarmer);
                if(savedFarmer!=null)
                    return gson.toJson(savedFarmer);
            }
        }
        return getGenericResponse("");
    }

    private boolean updateFarmerDevices(Farmer farmer,String deviceId){
        boolean deviceSaved=false;
        if(farmer!=null && farmer.getId()!=null && deviceId!=null && deviceId.trim().length()>0){
            deviceSaved = FarmerDao.updateFarmerDevice(farmer.getId(),deviceId);
        }
        return deviceSaved;
    }

    private boolean saveContentFromString(String fileString, String filePath){
        boolean fileSaved = false;
        System.out.println("FilePath : " +filePath);
        if(fileString!=null && fileString.trim().length()>0){
            OutputStream stream = null;
            try {

                byte[] data = DatatypeConverter.parseBase64Binary(fileString);
                File file = new File(filePath);
                file.createNewFile();
                stream = new FileOutputStream(file);
                stream.write(data);
                return true;
            } catch (Exception e){
                if(stream!=null)
                    try {
                        stream.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                System.out.println("Exception while converting image from string"+e);
            }
        }
        return fileSaved;
    }

    private FarmerQuery constructFarmerQueryObject(Farmer farmer,FarmerQuery farmerQuery){
        FarmerQuery query = null;
        if(farmer!=null && farmerQuery!=null){
            query = new FarmerQuery();
            query.setFarmerId(farmer.getId());
            query.setQueryString(farmerQuery.getQueryString());
            query.setCreatedOn(new Date());
            if(farmerQuery.getImageString()!=null && !farmerQuery.getImageString().isEmpty()){
                query.setqImageAvail(true);
            }
            if(farmerQuery.getAudioString()!=null && !farmerQuery.getAudioString().isEmpty())
                query.setqAudioAvail(true);
        }
        return query;
    }
}
