package com.farmbook.model;

import java.util.List;

public class PushNotificationRequest {
 public List<String> getRegistration_ids() {
  return registration_ids;
 }

 public void setRegistration_ids(List<String> registration_ids) {
  this.registration_ids = registration_ids;
 }

 public PushNotificationData getData() {
  return data;
 }

 public void setData(PushNotificationData data) {
  this.data = data;
 }

 private List<String> registration_ids;
 private PushNotificationData data;
}
