package com.farmbook.model;

public class PushNotificationData {

    FarmerQuery query;

    public FarmerQuery getQuery() {
        return query;
    }

    public void setQuery(FarmerQuery query) {
        this.query = query;
    }
}
