package com.farmbook.model;

import java.util.Date;

public class FarmerQuery extends BaseModel {

    Long id;
    String queryString;
    String imageString;
    String audioString;
    boolean qImageAvail;
    boolean qAudioAvail;
    Long farmerId;
    String solution;
    boolean sImageAvail;
    boolean sAudioAvail;
    Long expertId;
    Date createdOn;
    Date answeredOn;
    Farmer farmer;
    String sImageUrl;
    String sAudioUrl;
    String qImageUrl;
    String qAudioUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    public boolean isqImageAvail() {
        return qImageAvail;
    }

    public void setqImageAvail(boolean qImageAvail) {
        this.qImageAvail = qImageAvail;
    }

    public boolean isqAudioAvail() {
        return qAudioAvail;
    }

    public void setqAudioAvail(boolean qAudioAvail) {
        this.qAudioAvail = qAudioAvail;
    }

    public Long getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(Long farmerId) {
        this.farmerId = farmerId;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public boolean issImageAvail() {
        return sImageAvail;
    }

    public void setsImageAvail(boolean sImageAvail) {
        this.sImageAvail = sImageAvail;
    }

    public boolean issAudioAvail() {
        return sAudioAvail;
    }

    public void setsAudioAvail(boolean sAudioAvail) {
        this.sAudioAvail = sAudioAvail;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getAnsweredOn() {
        return answeredOn;
    }

    public void setAnsweredOn(Date answeredOn) {
        this.answeredOn = answeredOn;
    }

    public String getAudioString() {
        return audioString;
    }

    public void setAudioString(String audioString) {
        this.audioString = audioString;
    }

    public Farmer getFarmer() {
        return farmer;
    }

    public void setFarmer(Farmer farmer) {
        this.farmer = farmer;
    }

    public String getsImageUrl() {
        return sImageUrl;
    }

    public void setsImageUrl(String sImageUrl) {
        this.sImageUrl = sImageUrl;
    }

    public String getsAudioUrl() {
        return sAudioUrl;
    }

    public void setsAudioUrl(String sAudioUrl) {
        this.sAudioUrl = sAudioUrl;
    }

    public String getqImageUrl() {
        return qImageUrl;
    }

    public void setqImageUrl(String qImageUrl) {
        this.qImageUrl = qImageUrl;
    }

    public String getqAudioUrl() {
        return qAudioUrl;
    }

    public void setqAudioUrl(String qAudioUrl) {
        this.qAudioUrl = qAudioUrl;
    }
}
