package com.farmbook.util;

public class Constants {

    public static final String IMAGE_FORMAT = ".jpg";
    public static final String AUDIO_FORMAT = ".mp3";
    //public static final String Q_AUDIO_FORMAT = ".mp3";


    public static String getResourcePath(){
        if(System.getProperty("os.name").toLowerCase().contains("win")) return "";
        return "/home/ubuntu/farmbook";
    }

    public static String getQueryImagePath(){
        return getResourcePath()+"/images/query/";
    }

    public static String getQueryAudioPath(){
        return getResourcePath()+"/audio/query/";
    }


    public static String getSolutionImagePath(){
        return getResourcePath()+"/images/solution/";
    }

    public static String getSolutionAudioPath(){
        return getResourcePath()+"/audio/solution/";
    }

    public static String getQueryImageUrl(long id){
        return "/images/query/"+id+IMAGE_FORMAT;
    }

    public static String getSolutionImageUrl(long id){
        return "/images/solution/"+id+IMAGE_FORMAT;
    }

    public static String getQueryAudioUrl(long id){
        return "/audio/query/"+id+AUDIO_FORMAT;
    }

    public static String getSolutionAudioUrl(long id){
        return "/audio/solution/"+id+AUDIO_FORMAT;
    }
}
