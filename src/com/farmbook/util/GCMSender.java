package com.farmbook.util;

import com.farmbook.model.PushNotificationRequest;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

class GCMSender {

    String gcmURL = "https://android.googleapis.com/gcm/send";
    String appId = "AIzaSyB0RcbAxx8nYb2F8F8hFTpHyf7Aqwa5Yxc";

    public void sendNotice(PushNotificationRequest request){
        if(request!=null && request.getRegistration_ids()!=null && !request.getRegistration_ids().isEmpty()){
            System.out.println("Sending Notif");
            String gcmResponse = pushNotification(request);
            System.out.println("GCM Response "+gcmResponse);
        } else {
            System.out.println("Notif Request is "+request);
        }
    }

    String pushNotification(PushNotificationRequest request){
        Gson gson = new Gson();
        String json = gson.toJson(request);
        System.out.println("To JSON "+json);
        return pushToGCM(gcmURL, json,"application/json","key="+appId);
    }

    String pushToGCM(String url,String jsonData,String contentType,String auth){
        try {
            HttpEntity httpEntity = new StringEntity(jsonData);
            HttpPost method = new HttpPost(url);
            method.setEntity(httpEntity);
            method.setHeader("Content-Type", contentType);
            method.setHeader("Authorization", auth);
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse response = httpClient.execute(method);
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null)
                return EntityUtils.toString(resEntity);
        } catch (Exception e) {
            System.out.println("Error while pushing notification to GCM "+e);
        }
        return "Error while sending push notif for GCM request "+jsonData+" to url:"+url;
    }
}
