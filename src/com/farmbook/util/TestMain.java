package com.farmbook.util;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;

/**
 * Created by anil on 16/5/16.
 */
public class TestMain {

    public static void main2(String args[]) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        File audio = new File("/home/anil/Desktop/myaudio.3gp");
        FileInputStream fis =  new FileInputStream(audio);
        byte[] buf = new byte[1024];
        int n;
        while (-1 != (n = fis.read(buf)))
            baos.write(buf, 0, n);
        BASE64Encoder base64 = new BASE64Encoder();
        String fileAsStr = base64.encode(baos.toByteArray());
        System.out.println(fileAsStr);
        fis.close();
    }

    public static void main(String args[]) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        File audio = new File("/home/anil/Desktop/myAudioString.txt");
        FileInputStream fis =  new FileInputStream(audio);
        byte[] buf = new byte[1024];
        int n;
        while (-1 != (n = fis.read(buf)))
            baos.write(buf, 0, n);
        String fileAsStr = baos.toString();
        BASE64Decoder base64 = new BASE64Decoder();
        byte[] decodeBuffer = base64.decodeBuffer(fileAsStr);
        fis.close();


        File newAudio = new File("/home/anil/Desktop/newAudio.3gp");
        FileOutputStream fileOutputStream =  new FileOutputStream(newAudio);
        fileOutputStream.write(decodeBuffer);
        fileOutputStream.flush();
        fileOutputStream.close();
    }
}
