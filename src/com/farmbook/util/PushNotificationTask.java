package com.farmbook.util;

import com.farmbook.model.PushNotificationRequest;

import java.util.TimerTask;

public class PushNotificationTask extends TimerTask {
    public PushNotificationTask(PushNotificationRequest request) {
        this.request = request;
    }

    @Override
    public void run() {
        GCMSender sender = new GCMSender();
        sender.sendNotice(request);
    }

    public PushNotificationRequest getRequest() {
        return request;
    }

    public void setRequest(PushNotificationRequest request) {
        this.request = request;
    }

    private PushNotificationRequest request;
}
